function varargout = SpongeInMotionV2(varargin)
% SPONGEINMOTIONV2 M-file for SpongeInMotionV2.fig
%      SPONGEINMOTIONV2, by itself, creates a new SPONGEINMOTIONV2 or raises the existing
%      singleton*.
%
%      H = SPONGEINMOTIONV2 returns the handle to a new SPONGEINMOTIONV2 or the handle to
%      the existing singleton*.
%
%      SPONGEINMOTIONV2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SPONGEINMOTIONV2.M with the given input arguments.
%
%      SPONGEINMOTIONV2('Property','Value',...) creates a new SPONGEINMOTIONV2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before SpongeInMotionV2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to SpongeInMotionV2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help SpongeInMotionV2

% Last Modified by GUIDE v2.5 27-May-2015 13:18:10

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SpongeInMotionV2_OpeningFcn, ...
                   'gui_OutputFcn',  @SpongeInMotionV2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before SpongeInMotionV2 is made visible.
function SpongeInMotionV2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to SpongeInMotionV2 (see VARARGIN)

% Choose default command line output for SpongeInMotionV2
handles.output = hObject;

handles.cmap=flipud(colormap('gray'));
% handles.cmap=(colormap('gray'));
axes(handles.axes)
axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_ROI1)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_ROI2)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_ROI1bw)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_ROI2bw)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_corrR1)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_corrR2)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% axes(handles.axes_distance)
% axis off, cla
% set(gca, 'Fontname','Arial', 'Fontsize', 6);
% Calibration factors
% 2 mm equals (measured by plotting TIF files in Matlab figure)
% 
%--------------------------------------------------------------------------
% Zeiss Stemi 2000
% 50x 402 pixels
% 40x 321 pixels
% 32x 255 pixels
% 25x 205 pixels
% handles.Imcal=[402 321 255 205]/2; % calfac 1 mm
% set(handles.list_magn,'String',[{'50'},{'40'},{'32'},{'25'}]);
% cd('C:\Documents and Settings\lyrebird\My Documents\DATA\workloop Toadfish MBL09')

%--------------------------------------------------------------------------
% Leica MZf165 0.5x planapo and 0.5x phototube
handles.Imcal=[]/2; % calfac 1 mm

set(handles.list_magn,'String',[{'0.73'},{'1.0'},{'1.25'},{'1.60'},{'2.0'},{'2.5'},{'3.2'},{'4.0'},{'5.0'},{'6.3'},{'8.0'},{'10.0'},{'12.0'}]);
set(handles.popup_lens,'String',[{'0.5x Planapo'},{'1.0x Planapo'}]);
rot_mag=[0.73, 1.0, 1.25, 1.60, 2.0, 2.50, 3.2, 4.0, 5.0, 6.3, 8.0, 10.0, 12.0];

handles.rot_mag=rot_mag;
handles.Imcal=rot_mag*31.372/2; % [pix] and rotary to [mm] calibrated 20/4/2010 with 0.5x APO
set(handles.text_framerate,'String','2')

cd('~/TETHYA_PROJECT/PHOTOS/')
handles.index=1;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes SpongeInMotionV2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SpongeInMotionV2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% movegui(hObject,[1681 90]);


% --- Executes on selection change in list_magn.
function list_magn_Callback(hObject, eventdata, handles)
% hObject    handle to list_magn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns list_magn contents as cell array
%        contents{get(hObject,'Value')} returns selected item from list_magn

clc
% get the structure in the subfunction
handles     =   guidata(hObject);
frame1     =   255-handles.frame1;
Imcal     =   handles.Imcal;
cmap=handles.cmap;
magn_i=get(handles.list_magn,'Value');
pic_size=size(frame1);
scale_len=Imcal(magn_i)

axes(handles.axes)
cla
hold on
imagesc(imadjust(frame1))
colormap(cmap)
line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
axis image off


% --- Executes during object creation, after setting all properties.
function list_magn_CreateFcn(hObject, eventdata, handles)
% hObject    handle to list_magn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in push_getfile.
function push_getfile_Callback(hObject, eventdata, handles)
% hObject    handle to push_getfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc
% get the structure in the subfunction
handles     =   guidata(hObject);
dirname     =   handles.dirname;
% cmap        =   handles.cmap;
% Imcal       =   handles.Imcal;
% 
% magn_i=get(handles.list_magn,'Value');
% [FileName,PathName]=uigetfile('.jpg','Choose file:');
% cd (PathName)
% 
% fileinfo = aviinfo(FileName)
% numframes = fileinfo.NumFrames;
% Fs = fileinfo.FramesPerSecond;
% 
% set(handles.slider_framenum,'Value',1)
% set(handles.slider_framenum,'Min',1)
% set(handles.slider_framenum,'Sliderstep',[1/numframes 10/numframes])
% set(handles.slider_framenum,'Max',numframes)
% set(handles.text_framerate,'String',num2str(Fs))
% set(handles.text_numframes,'String',num2str(numframes))
% 
% mov = aviread(FileName, 1);
% pic_size=size(mov.cdata);
% scale_len=Imcal(magn_i);
% 
% axes(handles.axes)
% cla
% hold on
% frame1=mov.cdata(:,:,1);
% imshow(frame1)
% colormap(gray)
% line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
% text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
% text(10 ,pic_size(1)-10,['frame: ' num2str(1) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
% axis image on xy
% 
% frame1=mov.cdata;
% if get(handles.radio_plotfigs,'Value')==1
%     figure(6)
%     cla
%     set(gcf,'Name','Highspeed Recording','Position',[2435 656 920 318])
%     hold on
%     imshow(frame1)
%     colormap(gray)
%     colorbar
%     line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
%     text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
%     text(10 ,pic_size(1)-10,['frame: ' num2str(1) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
%     axis image on xy
% end
% 
% 
% % Update handles structure
% handles.FileName=FileName;
% handles.numframes=numframes;
% handles.frame1=mov.cdata;
% guidata(hObject, handles);
% 

% --- Executes on button press in push_setdir.
function push_setdir_Callback(hObject, eventdata, handles)
% hObject    handle to push_setdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% set directory & get filenames
clc
handles     =   guidata(hObject);
% cmap        =   handles.cmap;
% Imcal       =   handles.Imcal;
% magn_i      =   get(handles.list_magn,'Value');
% Fs          =   str2num(get(handles.text_framerate,'String'));
s           =   pwd;    % Get current directory
dirname     =   uigetdir('/Users/dmills/Documents/PhD Thesis/TETHYA PROJECT/PHOTOS')

cd(dirname);
k   =   find(dirname=='/');
dirname(k(end)+1:end);
set(handles.text_dirname2, 'String', dirname(k(end)+1:end) );

FileNames=dir('*.JPG');
numframes=numel(FileNames)
 
% fileinfo = aviinfo(FileName)
% numframes = fileinfo.NumFrames;
% Fs = fileinfo.FramesPerSecond;

set(handles.slider_framenum,'Value',1)
set(handles.slider_framenum,'Min',1)
set(handles.slider_framenum,'Sliderstep',[1/numframes 10/numframes])
set(handles.slider_framenum,'Max',numframes)
% set(handles.text_framerate,'String',num2str(Fs))
set(handles.text_numframes,'String',num2str(numframes))

A = imread(FileNames(1).name);

pic_size=size(A);
pic_size=pic_size(1:2);

% scale_len=Imcal(magn_i);

axes(handles.axes)
cla
hold on
imshow(A)
colormap(gray)
% line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
% text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
text(10 ,pic_size(1)-10,['frame: ' num2str(1) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
axis image on ij

% frame1=mov.cdata;
if get(handles.radio_plotfigs,'Value')==1
    figure(6)
    cla
%     set(gcf,'Name','Highspeed Recording','Position',[2435 656 920 318])
    hold on
    imshow(A)
    colormap(gray)
    colorbar
%     line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
%     text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
    text(10 ,pic_size(1)-10,['frame: ' num2str(1) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
    axis image on ij
end


% Update handles structure
handles.dirname=dirname;
handles.FileNames=FileNames;
handles.numframes=numframes;
handles.frame1=A;
guidata(hObject, handles);



% --- Executes on button press in push_markers.
function push_markers_Callback(hObject, eventdata, handles)
% hObject    handle to push_markers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc
% get the structure in the subfunction
handles     =   guidata(hObject);
frame1      =   255-handles.frame1;
Imcal       =   handles.Imcal;
cmap        =   handles.cmap;
magn_i      =   get(handles.list_magn,'Value');
ROIsize     =   round(str2num(get(handles.edit_ROIsize,'String')))
markersize  =   round(str2num(get(handles.edit_markersize,'String')))

pic_size=size(frame1);
scale_len=Imcal(magn_i);

%% draw box 
axes(handles.axes)
axis image on 

[I rect]=imcrop;

axes(handles.axes9)
axis image on 
imshow(I)

handles.rect=rect;

% 
% handles.ROIsize=ROIsize;
% handles.markersize=markersize;
% handles.I1_marker=I1_marker;
% handles.I2_marker=I2_marker;
% handles.rect1_marker=rect_marker;
% handles.rect2_marker=rect2_marker;
% handles.x1_start=xpos;
% handles.y1_start=ypos;
% handles.x2_start=x2pos;
% handles.y2_start=y2pos;
handles.index=1;
guidata(hObject, handles);


% --- Executes on selection change in popup_nummarkers.
function popup_nummarkers_Callback(hObject, eventdata, handles)
% hObject    handle to popup_nummarkers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popup_nummarkers contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_nummarkers


% --- Executes during object creation, after setting all properties.
function popup_nummarkers_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_nummarkers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in push_Sequence.
function push_Sequence_Callback(hObject, eventdata, handles)
% hObject    handle to push_Sequence (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles     =   guidata(hObject);
dirname     =   handles.dirname;
FileNames    =   handles.FileNames;
numframes   =   handles.numframes;
dirname=handles.dirname;

Imcal       =   handles.Imcal;
cmap        =   handles.cmap;
magn_i      =   get(handles.list_magn,'Value');
framerate   =   str2num(get(handles.text_framerate,'String'))
scale_len   =   Imcal(magn_i);
ROIsize     =   round(str2num(get(handles.edit_ROIsize,'String')));
markersize  =   round(str2num(get(handles.edit_markersize,'String')));


cd(dirname)
mkdir('segmented')
cd('segmented')
segdir=pwd;

clc

i=1;
%% START LOOP
tic
% h = waitbar(0,'Please wait...');
% set(h,'Position',[ 1700 50 350 50])
    
delbord=300;

Sp=[];

for j=1:numframes
    cd(dirname)
    A = imread(FileNames(j).name);

    B=A(delbord:end,:,1); % red channel

    BKG=mean(mean(A(1:200,1:200,1)));
    B=B-BKG;
    if get(handles.radio_levelaut,'Value')==1
        level = multithresh(B)
        seg_I = imquantize(B,level+BKG);
        seg_I=seg_I-1;
    else
        level = str2num(get(handles.edit_level1, 'String'));
        seg_I = imquantize(B,level+BKG);
        seg_I=seg_I-1;    
    end
    
    remarea = str2num(get(handles.edit_level2, 'String'));
    bw = bwareaopen(seg_I,remarea);
    BWnobord = imclearborder(bw, 4);
    % figure, imshow(BWnobord)
    BWdfill = imfill(BWnobord, 'holes');
    bw = bwareaopen(BWdfill,remarea);

    [B,L] = bwboundaries(bw,'noholes');

    stats = regionprops(L,'Area','Centroid');

    r=sqrt(stats.Area/pi);    
    
    Sp(i).area=stats.Area;
    Sp(i).r=r;
    Sp(i).centroid=stats.Centroid;
    
    drim=0; % draw images or not
    if get(handles.radiobutton5,'Value')==1
        figure(1)
        cla
        imshow(A)
        hold on
        % imshow(label2rgb(L, @jet, [.5 .5 .5]))
        hold on
        viscircles([stats.Centroid(1) stats.Centroid(2)+delbord] ,r,'EdgeColor',[1 .2 .2]); % circle red
        for k = 1
          boundary = B{k};
          plot(boundary(:,2), boundary(:,1)+delbord,'Color', [.8 .8 .8], 'LineWidth', 2)
        end

        plot(stats.Centroid(1),stats.Centroid(2)+delbord,'ro')
        line([stats.Centroid(1) stats.Centroid(1)+r],delbord+[stats.Centroid(2) stats.Centroid(2)], 'Color', [0.2 0.2 1 ],'Linewidth',2)  % readius blue

        text(2800, 50, [FileNames(j).name(1:end-4)], 'Color', [1 0 0 ])
        
        cd(segdir)
    %     imwrite(A,[FileNames(j).name(1:end-4) '_seg.jpg'],'jpg')
        print(gcf,'-djpeg',[FileNames(j).name(1:end-4) '_seg.jpg'])
    end
    
% waitbar(j/numframes);
i=i+1

end
cd(dirname)

%%

%     close (h)
toc

save ('Tethya results.mat', 'Sp', 'framerate')

assignin('base', 'Sp', Sp);

time=(1:numel(Sp))/framerate;
Area_sm = moving_average([Sp.area], 5);
Radius_sm = moving_average([Sp.r], 5);


axes(handles.axes_distance)
cla
hold on
plot(time,[Sp.r],'bo')
plot (time, Radius_sm,'k-')
ylabel('Radius')
xlabel('time (min)')
grid on
axis on

figure (2)
% subplot 211
% hold on
% plot(time,[Sp.r],'.', 'MarkerEdgeColor', [.2 .2 1])
% plot (time, Radius_sm,'k-')
% ylabel('Radius [pixels]')
% xlabel('time (s)')
% grid on
% axis on
% 
% subplot 212
hold on
plot(time,[Sp.area]/1e6,'.', 'MarkerEdgeColor', [1 .2 .2])
plot (time, Area_sm/1e6,'k-')
ylabel('Area [Mpixels]')
xlabel('time (min)')
grid on
axis on
print(gcf,'-djpeg',['Tethya results_data.jpg'])

s=pwd;
% save([FileName(1:end-4) '.mat'], 'length_mm','length_pix', 'scale_len', 'time', 'framerate')
% 
% [FileName,PathName] = uiputfile('.mat','Save file as:' ,[FileName(1:end-4) '.mat']) 
% cd(PathName)
% save(FileName, 'length_mm','length_pix', 'scale_len', 'time', 'framerate')
guidata(hObject, handles);



function edit_ROIsize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_ROIsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_ROIsize as text
%        str2double(get(hObject,'String')) returns contents of edit_ROIsize as a double


% --- Executes during object creation, after setting all properties.
function edit_ROIsize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_ROIsize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_framenum_Callback(hObject, eventdata, handles)
% hObject    handle to slider_framenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

handles     =   guidata(hObject);
FileNames   =   handles.FileNames;
numframes   =   handles.numframes;
cmap        =   handles.cmap;
frnum       =   round(get(handles.slider_framenum,'Value'));
set(handles.slider_framenum,'Value',frnum);
Imcal       =   handles.Imcal;
magn_i      =   get(handles.list_magn,'Value');
% rect=handles.rect;

A = imread(FileNames(frnum).name);
pic_size=size(A);
pic_size=pic_size(1:2);

scale_len=Imcal(magn_i);

axes(handles.axes)
cla
hold on
imshow(A)
% colormap(gray)
line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)

text(10 ,pic_size(1)-10,['frame: ' num2str(frnum) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
axis image off

% if handles.index
%     axes(handles.axes9)
%     cla
%     imshow(imcrop(A,handles.rect))
%     axis image off
% end

if get(handles.radio_plotfigs,'Value')==1
    figure(1)
    cla
    hold on
    imshow(A)
    % colormap(gray)
    line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
    text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)

    text(10 ,pic_size(1)-10,['frame: ' num2str(frnum) ' / ' num2str(numframes)] ,'Color',[1 0 0],'FontSize',10)
axis image off
end


% Update handles structure
% handles.FileNames=FileName;
handles.numframes=numframes;
handles.frame1=A;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function slider_framenum_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_framenum (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in push_getmarkers.
function push_getmarkers_Callback(hObject, eventdata, handles)
% hObject    handle to push_getmarkers (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc
% get the structure in the subfunction
handles     =   guidata(hObject);
A           =   handles.frame1;
Imcal       =   handles.Imcal;
cmap        =   handles.cmap;
magn_i      =   get(handles.list_magn,'Value');
ROIsize     =   round(str2num(get(handles.edit_ROIsize,'String')));
markersize  =   round(str2num(get(handles.edit_markersize,'String')));
% pic_size    =   size(A);
scale_len   =   Imcal(magn_i);

% claheI = adapthisteq(B,'NumTiles',[10 10]);
% claheI = imadjust(claheI);
% imshow(claheI)

% [centers, radii] = imfindcircles(rgb,[900 1300],'Sensitivity',0.9) % ,'ObjectPolarity','dark')      %#ok<NASGU,ASGLU> Variables 'centers' and 'radii' are needed to display the output with proper names.
% h = viscircles(centers,radii);
delbord=300;
B=A(delbord:end,:,1); % red channel
BKG=mean(mean(A(1:200,1:200,1)))
B=B-BKG;

if get(handles.radio_levelaut,'Value')==1
    level = multithresh(B)
    seg_I = imquantize(B,level+BKG);
    seg_I=seg_I-1;
else
    level = str2num(get(handles.edit_level1, 'String'))
    seg_I = imquantize(B,level+BKG);
    seg_I=seg_I-1;
end

remarea = str2num(get(handles.edit_level2, 'String'));
bw = bwareaopen(seg_I,remarea);
BWnobord = imclearborder(bw, 4);
% figure, imshow(BWnobord)
% BWdfill = imfill(BWnobord, 'holes');
BWdfill = imfill(bw, 'holes');
bw = bwareaopen(BWdfill,remarea);

[B,L] = bwboundaries(bw,'noholes');

stats = regionprops(L,'Area','Centroid')
stats.Area

r=sqrt(stats.Area/pi);

if get(handles.radio_plotfigs,'Value')==1
    figure(1)
else
    axes(handles.axes9)
end
cla
imshow(A)
hold on
% imshow(label2rgb(L, @jet, [.5 .5 .5]))
hold on
viscircles([stats.Centroid(1) stats.Centroid(2)+delbord] ,r,'EdgeColor',[1 .5 .5]);
for k = 1
  boundary = B{k};
  plot(boundary(:,2), boundary(:,1)+delbord,'Color', [1 1 1 ], 'LineWidth', 2)
end

plot(stats.Centroid(1),stats.Centroid(2)+delbord,'ro')
plot(stats.Centroid(1)+r,stats.Centroid(2)+delbord,'bo')

line([stats.Centroid(1) stats.Centroid(1)+r],[stats.Centroid(2)+delbord stats.Centroid(2)+delbord], 'Color', [0.2 0.2 1 ],'Linewidth',2)  % readius blue


% 
% I1_marker=handles.I1_marker;
% I2_marker=handles.I2_marker;
% rect1_marker=handles.rect1_marker;
% rect2_marker=handles.rect2_marker;
% x1_start=handles.x1_start;
% y1_start=handles.y1_start;
% x2_start=handles.x2_start;
% y2_start=handles.y2_start;
% 
% 
% %% Correlation MARKER 1 in new ROI 1 around initial position
% clear c imax max_c
% 
% rect_ROI = [x1_start-.5*ROIsize y1_start-.5*ROIsize ROIsize ROIsize];
% I1_ROI = imcrop(frame1,rect_ROI);       
% c = normxcorr2(I1_marker,I1_ROI);
% 
% % offset found by correlation
% [max_c, imax] = max(abs(c(:)));
% [ypeak, xpeak] = ind2sub(size(c),imax(1));
% corr_offset = [(xpeak-size(I1_marker,2))
%                (ypeak-size(I1_marker,1))];
% 
% % relative offset of position of subimages
% rect_offset = [(rect_ROI(1)-rect1_marker(1))
%                (rect_ROI(2)-rect1_marker(2))];
% 
% % total offset
% offset = corr_offset + rect_offset;
% xoffset = offset(1);
% yoffset = offset(2);
% xpos=x1_start+round(xoffset)
% ypos=y1_start+round(yoffset)
% 
% 
% % plot data
% axes(handles.axes_ROI1)
% cla
% imagesc(I1_marker)
% hold on
% plot (.5*markersize+1, .5*markersize+1,'g.')
% axis image on xy
% 
% axes(handles.axes_corrR1) 
% cla
% hold on
% surf(c), colormap (gca, cmap), shading interp
% plot (xpeak, ypeak, 'go')
% view (0,0)
% set (gca,'ZLim',[0 1])
% axis on
% 
% axes(handles.axes)
% hold on
% plot(xpos,ypos,'g.');
% 
% if get(handles.radio_plotfigs,'Value')==1
%     figure (6)
%     clf
%     set(gcf,'Position',[2435 656 920 318])
%     hold on
%     plot (xpos,ypos,'g.')
% 
%     figure (8)
%     clf
%     set(gcf,'Name','Marker extraction: ROI 2 ','Position',[9   34   921   458])
%     subplot 221
%     hold on
%     imagesc(I1_ROI)  
%     plot (round(corr_offset(1))+1+.5*markersize, round(corr_offset(2))+1+.5*markersize,'g.')
%     colormap (cmap)
%     colorbar
%     axis image on xy
%     title ('Marker 1; Xcorr in ROI1')
% 
%     subplot 223
%     hold on
%     imagesc(I1_marker)  
%     plot (.5*markersize+1, .5*markersize+1,'g.')
%     colormap (cmap)
%     colorbar
%     axis image on xy
% end
% 
% 
% 
% 
% %% Correlation MARKER 2 in new ROI 2 around initial position
% clear c imax max_c
% 
% % non-interactively
% rect2_ROI = [x2_start-.5*ROIsize y2_start-.5*ROIsize ROIsize ROIsize];
% I2_ROI = imcrop(frame1,rect2_ROI);
% c = normxcorr2(I2_marker,I2_ROI);
% % offset found by correlation
% [max_c, imax] = max(abs(c(:)));
% [y2peak, x2peak] = ind2sub(size(c),imax(1));
% corr2_offset = [(x2peak-size(I2_marker,2))
%                (y2peak-size(I2_marker,1))];
% 
% % relative offset of position of subimages
% rect2_offset = [(rect2_ROI(1)-rect2_marker(1))
%                (rect2_ROI(2)-rect2_marker(2))];
% 
% % total offset
% offset2 = corr2_offset + rect2_offset;
% x2offset = offset(1);
% y2offset = offset(2);
% 
% x2pos=x2_start+round(x2offset)
% y2pos=y2_start+round(y2offset)
% 
% 
% % plot figures marker 2
% axes(handles.axes_ROI2)
% cla
% imagesc(I2_marker)
% hold on
% plot (.5*markersize+1, .5*markersize+1,'r.')
% axis image on xy
% 
% axes(handles.axes_corrR2) 
% cla
% hold on
% surf(c), colormap (gca, cmap), shading interp
% plot (x2peak, y2peak, 'ro')
% view (0,0)
% set (gca,'ZLim',[0 1])
% axis on
% 
% axes(handles.axes)
% hold on
% plot(x2pos,y2pos,'r.');
% 
% if get(handles.radio_plotfigs,'Value')==1
%     
%     figure (6)
%     set(gcf,'Position',[2435 656 920 318])
%     hold on
%     plot (x2pos,y2pos,'r.')
% 
%     figure (8)
%     set(gcf,'Name','Marker extraction: ROI 2 ','Position',[9   34   921   458])
%     subplot 222
%     hold on
%     imagesc(I2_ROI)  
%     plot (round(corr2_offset(1))+1+.5*markersize, round(corr2_offset(2))+1+.5*markersize,'r.')
%     colormap (cmap)
%     colorbar
%     axis image on xy
%     title ('Marker 2; Xcorr in ROI1')
% 
%     subplot 224
%     hold on
%     imagesc(I2_marker)  
%     plot (.5*markersize+1, .5*markersize+1,'r.')
%     colormap (cmap)
%     colorbar
%     axis image on xy
% end

function edit_markersize_Callback(hObject, eventdata, handles)
% hObject    handle to edit_markersize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_markersize as text
%        str2double(get(hObject,'String')) returns contents of edit_markersize as a double


% --- Executes during object creation, after setting all properties.
function edit_markersize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_markersize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_framerate_Callback(hObject, eventdata, handles)
% hObject    handle to text_framerate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text_framerate as text
%        str2double(get(hObject,'String')) returns contents of text_framerate as a double


% --- Executes during object creation, after setting all properties.
function text_framerate_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_framerate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit_level1_Callback(hObject, eventdata, handles)
% hObject    handle to edit_level1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_level1 as text
%        str2double(get(hObject,'String')) returns contents of edit_level1 as a double


% --- Executes during object creation, after setting all properties.
function edit_level1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_level1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in radio_levelaut.
function radio_levelaut_Callback(hObject, eventdata, handles)
% hObject    handle to radio_levelaut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_levelaut



function edit_level2_Callback(hObject, eventdata, handles)
% hObject    handle to edit_level2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit_level2 as text
%        str2double(get(hObject,'String')) returns contents of edit_level2 as a double


% --- Executes during object creation, after setting all properties.
function edit_level2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit_level2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in push_LevelM1_min.
function push_LevelM1_min_Callback(hObject, eventdata, handles)
% hObject    handle to push_LevelM1_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in push_LevelM2_min.
function push_LevelM2_min_Callback(hObject, eventdata, handles)
% hObject    handle to push_LevelM2_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in push_LevelM1_plus.
function push_LevelM1_plus_Callback(hObject, eventdata, handles)
% hObject    handle to push_LevelM1_plus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in push_LevelM2_plus.
function push_LevelM2_plus_Callback(hObject, eventdata, handles)
% hObject    handle to push_LevelM2_plus (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in radio_plotfigs.
function radio_plotfigs_Callback(hObject, eventdata, handles)
% hObject    handle to radio_plotfigs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radio_plotfigs

if get(hObject,'Value') ==0
    h=figure(6); close (h);
    h=figure(7); close (h);
    h=figure(8); close (h);
    h=figure(9); close (h);
    h=figure(10); close (h);
end


% --- Executes on key press with focus on push_Sequence and none of its controls.
function push_Sequence_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to push_Sequence (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on selection change in popup_lens.
function popup_lens_Callback(hObject, eventdata, handles)
% hObject    handle to popup_lens (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popup_lens contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_lens

handles     =   guidata(hObject);
lens=get(handles.popup_lens,'Value');
rot_mag=handles.rot_mag;

if lens==1
    lensfac=.5;
elseif lens==2
    lensfac=1;
end

handles.Imcal=rot_mag*31.372*lensfac; % [pix] and rotary to [mm] calibrated 20/4/2010 with 0.5x APO
guidata(hObject, handles);

clc
% get the structure in the subfunction
frame1     =   255-handles.frame1;
Imcal     =   handles.Imcal;
cmap=handles.cmap;
magn_i=get(handles.list_magn,'Value');
pic_size=size(frame1);
scale_len=Imcal(magn_i)

axes(handles.axes)
cla
hold on
imagesc(imadjust(frame1))
colormap(cmap)
line([pic_size(2)-scale_len-20 pic_size(2)-20],[15 15],'Linewidth',2,'Color',[1 0 0]);
text(pic_size(2)-(.5*scale_len)-20,20,'1 mm','Color',[1 0 0],'FontSize',20)
axis image off



% --- Executes during object creation, after setting all properties.
function popup_lens_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_lens (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in push_measdist.
function push_measdist_Callback(hObject, eventdata, handles)
% hObject    handle to push_measdist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

clc
% get the structure in the subfunction
handles     =   guidata(hObject);
frame1      =   handles.frame1;
Imcal       =   handles.Imcal;
cmap        =   handles.cmap;
magn_i      =   get(handles.list_magn,'Value');

pic_size=size(frame1);
scale_len=Imcal(magn_i);

%% pick two markers
axes(handles.axes)
axis image on xy
[x,y] = ginput(2);

D=sqrt((x(2)-x(1))^2+(y(2)-y(1))^2);
length_mm=D/scale_len


% --- Executes during object creation, after setting all properties.
function push_setdir_CreateFcn(hObject, eventdata, handles)
% hObject    handle to push_setdir (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in radiobutton4.
function radiobutton4_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton4


% --- Executes on button press in radiobutton5.
function radiobutton5_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton5
