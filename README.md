## Data for paper: ##

Mills, D.B., W.R. Francis, S. Vargas, M. Larsen, C.P.H. Elemans, D.E. Canfield, G. W�rheide (2018) [The last common ancestor of animals lacked the HIF pathway and respired in low-oxygen environments.](https://elifesciences.org/articles/31176) *eLife* 7:e31176.

For more information about *Tethya wilhelma*, the genome, and annotations, see the associated [repository of the genome](https://bitbucket.org/molpalmuc/tethya_wilhelma-genome)

Repository can be cloned with git (use link above), or [downloaded as .zip](https://bitbucket.org/molpalmuc/sponge-oxygen/downloads/)

### contraction_traces ###
* three exemplary traces used for Figure 4
* the data matrix used for analysis
* the R script used to analyse the data

### differentially_expressed_genes ###
* down- and up-regulated genes for shock and long-term hypoxia as compared to controls
* annotated versions with top BLAST hit to [Amphimedon v2.1](http://amphimedon.qcloud.qcif.edu.au/downloads.html) and [Uniprot](http://www.uniprot.org/downloads#uniprotkblink)
* count matrices used in the study
* R scripts used to analyse the data

SRA links to raw transcriptome data can be found at [NCBI BioProject PRJNA380886](https://www.ncbi.nlm.nih.gov/bioproject/380886)

### gene_trees ###
FASTA format proteins, alignments and RAxML output files for:

* [HIFa](http://www.uniprot.org/uniprot/Q16665) (hypoxia-inducible factor) and other bHLH-PAS proteins across metazoa, which include [ARNT/HIFb](http://www.uniprot.org/uniprot/P27540), [CLOCK](http://www.uniprot.org/uniprot/O15516), [PERIOD](http://www.uniprot.org/uniprot/O15534), [NPAS](http://www.uniprot.org/uniprot/Q99742), [AHR](http://www.uniprot.org/uniprot/P35869), [CYCLE/BMAL](http://www.uniprot.org/uniprot/O00327), [Methoprene tolerant](http://www.uniprot.org/uniprot/Q9VYW2). Two homologs were identified in the filasterean *Capsaspora owczarzaki*, but no homologs were detected in the two genomes or [18 transcriptomes](https://www.ncbi.nlm.nih.gov/pubmed/27765632) of choanoflagellates.
* [EGL9](http://www.uniprot.org/uniprot/G5EBV0) (egg-laying defective 9), also called PHD (proline-hydroxylase domain) or HPH (HIF prolyl hydroxylase)
* [VHL](http://www.uniprot.org/uniprot/P40337) (Von-Hippel Lindau tumor suppressor protein)
* [FIH](http://www.uniprot.org/uniprot/Q9NWT6) (Factor Inhibiting HIF), also called HIF1aN (Hypoxia-inducible factor 1-alpha inhibitor)

* [CBS](http://www.uniprot.org/uniprot/P35520) (Cystathionine beta-synthase), also called Beta-thionase or Serine sulfhydrase
* [CTH](http://www.uniprot.org/uniprot/P32929) (Cystathionine gamma-lyase), also called Cysteine-protein sulfhydrase
* [SQOR](http://www.uniprot.org/uniprot/Q9Y6N5) (Sulfide:quinone oxidoreductase), also called sqrdl
* [ETHE1](http://www.uniprot.org/uniprot/O95571) (Ethylmalonic encephalopathy protein 1), also called Persulfide dioxygenase or Sulfur dioxygenase ETHE1
* [SUOX](http://www.uniprot.org/uniprot/P51687) (sulfite oxidase)

### oxygen_recordings ###
* Anoxia-Exposure Experiment : 1 raw oxygen trace
* High Oxygen Controls : 4-5 raw traces each for 3 experiments
* Low Oxygen Experiments : 4-5 raw traces each for 4 experiments, including end-experiment calibration, and calculations as .xlsx files
* Respiration Kinetics Experiments : 1 raw trace and calculations for each of 3 experiments

All files can be unzipped with either `tar -zxfp` or `gunzip`.
