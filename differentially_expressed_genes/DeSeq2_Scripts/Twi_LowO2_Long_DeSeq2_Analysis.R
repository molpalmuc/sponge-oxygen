#http://www-huber.embl.de/users/klaus/Teaching/DESeq2Predoc2014.html#preparing-count-matrices consulted 07.03.2016

library("EDASeq")
library("DESeq2")
library("pheatmap")
library("ggplot2")
library("gplots")
library("geneplotter")
library("RColorBrewer")
library("genefilter")

library("vegan")

library("BiocParallel")
register(MulticoreParam(4))

#assuming you cloned the repo to a folder Repos in your /home directory
input_counts<-"~/Repos/sponge-oxygen/differentially_expressed_genes/Count_matrix/twi_combined_curatedgenes_mito.fasta.despliced_LongTreat.csv"

input_sample_information<-"~/Repos/sponge-oxygen/differentially_expressed_genes/Count_matrix/Twi_LowO2_Long.info"

DE_Matrix<-read.csv(input_counts, head=T, sep="\t")

DE_Matrix_INFO<-read.csv(input_sample_information, head=T)

# Remove genes that have no counts over all samples
DE_Matrix <- DE_Matrix[(rowSums(DE_Matrix) > 0),]

DE_MatrixSeq<-DESeqDataSetFromMatrix(countData=DE_Matrix, colData=DE_Matrix_INFO, design=~condition)

#relevel the factors so that control is the reference
DE_MatrixSeq$condition<-relevel(DE_MatrixSeq$condition,ref="Control")

#estimate size factors
#Thus, if all size factors are roughly equal to one, the libraries have been sequenced equally deeply.
DE_MatrixSeq<-estimateSizeFactors(DE_MatrixSeq)

#get rows with all non-zero counts
non_zero_rows<-apply(counts(DE_MatrixSeq), 1, function(x){all(x>0)})

#get all rows
all_rows<-apply(counts(DE_MatrixSeq), 1, function(x){all(x>=0)})

#number of non-zero rows:
sum(non_zero_rows)

#number of rows:
sum(all_rows)

#cummulative distribution of normalized counts for non-zero rows
multiecdf(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ], xlab="mean counts", xlim=c(0,1000))

#density of normalized counts
multidensity(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ], xlab="mean counts", xlim=c(0,1000))

#sample comparisons
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,2), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[2]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,3), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[3]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,4), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[4]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,5), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[5]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,6), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[6]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,7), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(1,8), main=paste( colnames(DE_MatrixSeq)[1], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,3), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[3]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,4), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[4]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,5), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[5]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,6), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[6]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,7), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(2,8), main=paste( colnames(DE_MatrixSeq)[2], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(3,4), main=paste( colnames(DE_MatrixSeq)[3], "vs.", colnames(DE_MatrixSeq)[4]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(3,5), main=paste( colnames(DE_MatrixSeq)[3], "vs.", colnames(DE_MatrixSeq)[5]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(3,6), main=paste( colnames(DE_MatrixSeq)[3], "vs.", colnames(DE_MatrixSeq)[6]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(3,7), main=paste( colnames(DE_MatrixSeq)[3], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(3,8), main=paste( colnames(DE_MatrixSeq)[3], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(4,5), main=paste( colnames(DE_MatrixSeq)[4], "vs.", colnames(DE_MatrixSeq)[5]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(4,6), main=paste( colnames(DE_MatrixSeq)[4], "vs.", colnames(DE_MatrixSeq)[6]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(4,7), main=paste( colnames(DE_MatrixSeq)[4], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(4,8), main=paste( colnames(DE_MatrixSeq)[4], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(5,6), main=paste( colnames(DE_MatrixSeq)[5], "vs.", colnames(DE_MatrixSeq)[6]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(5,7), main=paste( colnames(DE_MatrixSeq)[5], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(5,8), main=paste( colnames(DE_MatrixSeq)[5], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(6,7), main=paste( colnames(DE_MatrixSeq)[6], "vs.", colnames(DE_MatrixSeq)[7]))
MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(6,8), main=paste( colnames(DE_MatrixSeq)[6], "vs.", colnames(DE_MatrixSeq)[8]))

MDPlot(counts(DE_MatrixSeq, normalized=T)[non_zero_rows, ],c(7,8), main=paste( colnames(DE_MatrixSeq)[7], "vs.", colnames(DE_MatrixSeq)[8]))


#transform the data to rlog
DE_MatrixSeq_rlog<-rlogTransformation(DE_MatrixSeq, blind = T)

#PCA of samples
plotPCA(DE_MatrixSeq_rlog, intgroup=c("condition"))

#remove outlier samples:
#we need to one treatment
outliers=c("LowO2_Treat1_Concat_RNASeq_GCGCTG")

DE_MatrixSeq_NoOutliers<-DE_MatrixSeq[,!(colnames(DE_MatrixSeq) %in% outliers)]

#repeat above analyses
DE_MatrixSeq_NoOutliers_rlog<-rlogTransformation(DE_MatrixSeq_NoOutliers, blind = T)

#PCA of samples
plotPCA(DE_MatrixSeq_NoOutliers_rlog, intgroup=c("condition"))

####
#test for global differences in expression using the distance matrix and the function adonis.
#####

#produce a matrix with the transformed distances

o2_treatments<-data.frame(Condition=c("C","C","C","T","T","T","T"))

#now use the function adonis to test for differences between treatments. By default adonis uses the bray-curtis distance.
adonis(t(assay(DE_MatrixSeq_NoOutliers_rlog))~Condition, data=o2_treatments, method="euclidean")




###
#
#Differential expression analysis
#
#####
DE_MatrixSeq_NoOutliers<-estimateSizeFactors(DE_MatrixSeq_NoOutliers)
DE_MatrixSeq_NoOutliers<-estimateDispersions(DE_MatrixSeq_NoOutliers)

plotDispEsts(DE_MatrixSeq_NoOutliers)

#wald test
DE_MatrixSeq_NoOutliers<-nbinomWaldTest(DE_MatrixSeq_NoOutliers)
DE_MatrixSeq_NoOutliers_Results<-results(DE_MatrixSeq_NoOutliers, pAdjustMethod = "BH")

#number of DE genes at 0.01 significance level
table(DE_MatrixSeq_NoOutliers_Results$padj < 0.01)

#number of DE genes at 0.01 significance level and log fold change >= 2
table(DE_MatrixSeq_NoOutliers_Results$padj < 0.01 & abs(DE_MatrixSeq_NoOutliers_Results$log2FoldChange) >= 1)

#number of overexpressed genes at 0.01 significance level and log fold change >= 2
table(DE_MatrixSeq_NoOutliers_Results$padj < 0.01 & DE_MatrixSeq_NoOutliers_Results$log2FoldChange >= 1)

#number of underexpressed genes at 0.01 significance level and log fold change <= -2
table(DE_MatrixSeq_NoOutliers_Results$padj < 0.01 & DE_MatrixSeq_NoOutliers_Results$log2FoldChange <= -1)

#plot of p values
hist(DE_MatrixSeq_NoOutliers_Results$pvalue, main = "Treatment vs. Control", xlab="p-values")

plotMA(DE_MatrixSeq_NoOutliers_Results, alpha=0.01)

#get significant genes names; uncomment the write.csv lines and change the path if write to a file is required/wanted
#degs_names<-rownames(subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01 & abs(log2FoldChange) >= 2))
#write.csv(degs_names, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_all_degs_p001_2LFC.csv")

#get significantly overexpressed in treatment:
#over_deg_all_info<-subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01 & log2FoldChange >= 2)
#over_deg_names<-rownames(over_deg_all_info)
#write.csv(over_deg_all_info, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_over_degs_p001_2LFC_all_info.csv")
#write.csv(over_deg_names, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_over_degs_p001_2LFC.csv")

#get significantly underexpressed in treatment:
#under_deg_all_info<-subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01 & log2FoldChange <= -2)
#under_deg_names<-rownames(under_deg_all_info)
#write.csv(under_deg_all_info, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_under_degs_p001_2LFC_all_info.csv")
#write.csv(under_deg_names, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_under_degs_p001_2LFC.csv")

#get significant genes with a LFC above/below 1
all_degs_allinfo<-subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01)
write.csv(all_degs_allinfo, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_all_degs_p001_allinfo.csv")

#get significantly overexpressed in treatment LFC 1:
#over_deg_allinfo_lfc1<-subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01 & log2FoldChange >= 1)
#write.csv(over_allinfo_names_lfc1, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_over_degs_p001_1LFC_allinfo.csv")

#get significantly underexpressed in treatment LFC 1:
#under_deg_allinfo_lfc1<-subset(DE_MatrixSeq_NoOutliers_Results, padj<0.01 & log2FoldChange <= -1)
#write.csv(under_deg_allinfo_lfc1, "~/Repos/sponge-oxygen/differentially_expressed_genes/Twi_LongTreat_under_degs_p001_1LFC_allinfo.csv")

